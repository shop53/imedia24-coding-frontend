import './App.css';
import { BrowserRouter as Router, Routes, Route, useRoutes} from 'react-router-dom';
import Pokemon from './components/pokemon/Pokemon'
import PokemonList from './components/pokemon/PokemonList'
import Navbar from './components/layout/Navbar'

function AppRoutes() {
  const routes = useRoutes(
    [
      {path:'/',element:<PokemonList/>},
      {path:'/pokemon/:pokemon',element:<Pokemon/>}
    ]
  )
  return routes;
}
function App() {
  return (
    <div className="App">
        
        <Router>
          <Navbar/>
          <div class="container">
            <AppRoutes />
          </div>
        </Router>
    </div>
  );
}

export default App;
