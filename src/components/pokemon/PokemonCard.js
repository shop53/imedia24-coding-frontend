import React from "react";
import {Link} from "react-router-dom";

const PokemonCard = (props )=> {
    return(
        <div className="col-md-2 mt-4">
            <div className="card" >
                <img src={"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
                    props.data.url.split('pokemon/')[1].split('/')[0] +".png"} />
                <div className="card-body">
                    <h5 className="card-title">{props.data.name}</h5>
                    <Link to={`/pokemon/${props.data.name}`} state={props.data.name}>
                        Pokemon Details
                    </Link>
                </div>
            </div>
        </div>
    )
};

export default PokemonCard