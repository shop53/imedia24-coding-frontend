import React , { useState } from "react";
import {useDispatch, useSelector} from "react-redux";
import _ from "lodash";
import {GetPokemonList} from "../../reducers/actions/pokemonActions";
import PokemonCard from './PokemonCard';

const PokemonList = (props) => {
  const dispatch = useDispatch();
  const [index, setIndex] = useState(15);
  const pokemonList = useSelector(state => state.PokemonList);
  React.useEffect(() => {
    FetchData()
  }, []);

  const FetchData = () => {
    dispatch(GetPokemonList(index))
  }

  window.onscroll = () => {
    if (pokemonList.length > 150)
        return;
    if (
      window.innerHeight + document.documentElement.scrollTop ===
      document.documentElement.offsetHeight
    ) {
        dispatch(GetPokemonList(index+15))
        setIndex(index + 15);
    }
  };
  const ShowData = () => {
    if (!_.isEmpty(pokemonList.data)) {
      return(
        <div className={"row mt-4"}>
            {pokemonList && pokemonList.data.map(pokemon => (
                <PokemonCard data={pokemon}/>
            ))}
        </div>
      )
    }

    if (pokemonList.errorMsg !== "") {
      return <p>{pokemonList.errorMsg}</p>
    }

    return <p>unable to get data</p>
  };

  return(
    <div>
       {ShowData()}
    </div>
  )
};

export default PokemonList