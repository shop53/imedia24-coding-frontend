import { render, screen, cleanup} from '@testing-library/react'
import Pokemon from '../pokemon/Pokemon'
test('should render pokemon component', () => {
    render(<Pokemon/>);
    const pokemonElement = screen.getByTestId('pokemon-1')
    expect(pokemonElement).toBeInTheDocument();
})